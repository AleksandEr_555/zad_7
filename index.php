<?php

class Product {

	public $name;
	public $cena;
	public $massa;

	public function __construct($name, $cena, $massa) {

		$this->name = $name;
		$this->cena = $cena;
		$this->massa = $massa;
	}

	public function Print()
	{
		echo "<h3>$this->name: $this->massa, $this->cena</h3><hr>"; 
	}
}

$ananas = new Product('Ананас', '800 р.', '2 кг');
$apple = new Product('Яблоко', '400 р.', '3.6 кг');
$banan = new Product('Банан', '150 р.', '2 кг');

?>
<html>
<body>
	<center>
		<div style='width: 700px; height: 100px; margin-top: 10px; border: 2px solid black; background-color: orange'>
			<h1><b>Домашнее задание №7</b></h1>	
		</div>
		<div style='width: 700px; height: 500px; margin-top: 10px; border: 2px solid black; background-color: #F0E68C'>
			<?php 
				$ananas->Print();
				$apple->Print();
				$banan->Print();
			?>
		</div>
	</center>
</body>
</html>